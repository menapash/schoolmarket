//
//  AppDelegate.swift
//  SchoolMarket
//
//  Created by Marco PC on 09/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import GooglePlaces
import Photos

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    var indicator:ProgressIndicator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        GMSPlacesClient.provideAPIKey("AIzaSyBqyHvuQ1PnV15-Ym4xXF57L4eVKC2puWY")
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if GIDSignIn.sharedInstance().handle(url, sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [:]){
            
            return true
        }
        
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            print("\n\n\n",error)
            print("ciao")
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        let name = user.profile.name as! String
        let email = user.profile.email as! String
        let photoUrl = user.profile.imageURL(withDimension: 200)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                // ...
                return
            }
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            // User is signed in
            let uid = user?.uid as! String
            UserDefaults.standard.set(uid, forKey: "gUserUid")
            let db = Firestore.firestore()
            let docRef = db.collection("users").document(uid)
            docRef.getDocument { (document, error) in
                if (document?.exists)! {
                    let data = document?.data()
                    print("\n\n\n",data)
                    UserDefaults.standard.set(data!["email"] as! String, forKey: "userEmail")
                    UserDefaults.standard.set(data!["name"] as! String, forKey: "userName")
                    UserDefaults.standard.set(data!["surname"] as! String, forKey: "userSurname")
                    UserDefaults.standard.set(data!["city"] as! String, forKey: "userCity")
                    UserDefaults.standard.set(data!["lat"] as! String, forKey: "userLatitude")
                    UserDefaults.standard.set(data!["lng"] as! String, forKey: "userLongitude")
                    if(data!["isStudying"] as! Bool == true){
                        UserDefaults.standard.set(data!["isStudying"] as! Bool, forKey: "isStudying")
                        UserDefaults.standard.set(data!["student"] as! Bool, forKey: "student")
                        UserDefaults.standard.set(data!["specialization"] as! String, forKey: "userSpecialization")
                        UserDefaults.standard.set(data!["schoolLat"] as! String, forKey: "userSchoolLatitude")
                        UserDefaults.standard.set(data!["schoolLng"] as! String, forKey: "userSchoolLongitude")
                        if(data!["student"] as! Bool == false){
                            UserDefaults.standard.set(data!["school"] as! String, forKey: "userUniversity")
                        } else {
                            UserDefaults.standard.set(data!["school"] as! String, forKey: "userSchool")
                        }
                        
                    }
                    UserDefaults.standard.set(uid, forKey: "userUid")
                    
                    var storyboard = UIStoryboard(name: "Main", bundle: nil)
                    var viewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginVC
                    
                    self.window?.rootViewController = viewController
                    self.indicator = ProgressIndicator(inview:(self.window?.rootViewController?.view)!,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "Login...")
                    self.window?.rootViewController?.view.addSubview(self.indicator!)
                    
                    self.indicator!.start()
                    
                    let storage = Storage.storage()
                    let storageRef = storage.reference()
                    let riversRef = storageRef.child("profilePic/"+uid)
                    
                    self.indicator?.isHidden = false
                    self.indicator!.start()
                    
                    riversRef.downloadURL { url, error in
                        if let error = error {
                            // Handle any errors
                            //Upload as data
                            let upload = riversRef.putData(try! Data(contentsOf: photoUrl!), metadata: nil){ metadata, error in
                                if let error = error {
                                    // Uh-oh, an error occurred!
                                    print("Error: ",error)
                                    self.indicator!.stop()
                                    self.indicator?.isHidden = true
                                } else {
                                    // Metadata contains file metadata such as size, content-type, and download URL.
                                    print("Success")
                                    riversRef.downloadURL { url, error in
                                        if let error = error {
                                            // Handle any errors
                                            print("eroreeeeeeeeee: ",error)
                                            self.indicator!.stop()
                                            self.indicator!.isHidden = true
                                            self.window?.rootViewController?.performSegue(withIdentifier: "toHome", sender: self)
                                        } else {
                                            UserDefaults.standard.set(String(describing: url!), forKey: "userPhotoUrl")
                                            self.indicator!.stop()
                                            self.indicator!.isHidden = true
                                            self.window?.rootViewController?.performSegue(withIdentifier: "toHome", sender: self)
                                        }
                                    }
                                }
                            }
                        } else {
                            UserDefaults.standard.set(String(describing: url!), forKey: "userPhotoUrl")
                            self.indicator!.stop()
                            self.indicator!.isHidden = true
                            self.window?.rootViewController?.performSegue(withIdentifier: "toHome", sender: self)
                        }
                    }
                    
                } else {
                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Ti devi prima registrare per poter fare il login con Google", preferredStyle: UIAlertControllerStyle.alert);
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                        
                        var storyboard = UIStoryboard(name: "Main", bundle: nil)
                        var viewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginVC
                        
                        self.window?.rootViewController = viewController
                        self.window?.rootViewController?.performSegue(withIdentifier: "toRegistration", sender: self)
                    }
                    myAlert.addAction(okAction);
                    self.window?.rootViewController?.present(myAlert, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

