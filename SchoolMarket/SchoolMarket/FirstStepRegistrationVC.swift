//
//  FirstStepRegistrationVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 09/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase

class FirstStepRegistrationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UserDefaults.standard.bool(forKey: "googleBC")){
            name.text = UserDefaults.standard.string(forKey: "gUserName")
            email.text = UserDefaults.standard.string(forKey: "gUserEmail")
            email.isUserInteractionEnabled = false
        }
        
        name.delegate = self
        name.tag = 0
        surname.delegate = self
        surname.tag = 1
        email.delegate = self
        email.tag = 2
        password.delegate = self
        password.tag = 3
        
        nextButton.layer.cornerRadius = 5
        
        // Do any additional setup after loading the view.
    }

    @IBAction func nextStepBC(_ sender: Any) {
        
        //Se non è presente il testo colora i placeholders di rosso
        
        if email.text!.isEmpty || password.text!.isEmpty || repeatPassword.text!.isEmpty || name.text!.isEmpty || surname.text!.isEmpty {
            
            //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
            
            let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil);
            
            if email.text!.isEmpty{
                email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                email.layer.borderColor = UIColor.red.cgColor
            }
            if name.text!.isEmpty{
                name.attributedPlaceholder = NSAttributedString(string: "Nome", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                name.layer.borderColor = UIColor.red.cgColor
            }
            if surname.text!.isEmpty{
                surname.attributedPlaceholder = NSAttributedString(string: "Cognome", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                surname.layer.borderColor = UIColor.red.cgColor
            }
            if password.text!.isEmpty{
                password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                password.layer.borderColor = UIColor.red.cgColor
            }
            if repeatPassword.text!.isEmpty{
                repeatPassword.attributedPlaceholder = NSAttributedString(string: "Ripeti passsword", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                repeatPassword.layer.borderColor = UIColor.red.cgColor
            }
            
            //Se tutti i campi obbligatori sono stati inseriti, proseguo ad altri controlli
        }else{
            
            if(password.text! != repeatPassword.text!){
                
                //Alert per segnalare le due password diverse
                let myAlert = UIAlertController(title: "Attenzione\n", message: "Le due password devono coincidere", preferredStyle:UIAlertControllerStyle.alert);
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                myAlert.addAction(okAction);
                self.present(myAlert, animated: true, completion: nil);
                
            }else{
                if(UserDefaults.standard.bool(forKey: "googleBC")){
                    UserDefaults.standard.set(self.name.text!, forKey: "name")
                    UserDefaults.standard.set(self.surname.text!, forKey: "surname")
                    self.performSegue(withIdentifier: "nextStep", sender: self)
                } else {
                    Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (user, error) in
                        let uid = user?.uid
                        if error != nil {
                            
                            if let errCode = AuthErrorCode(rawValue: error!._code) {
                                
                                switch errCode {
                                case .invalidEmail:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Inserisci un indirizzo e-mail valido", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                case .emailAlreadyInUse:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Indirizzo e-mail già in uso", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                case .weakPassword:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "La password deve essere almeno di 6 caratteri", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                default:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Errore nella registrazione", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                }
                            }
                            
                        } else{
                            UserDefaults.standard.set(self.email.text!, forKey: "email")
                            UserDefaults.standard.set(self.name.text!, forKey: "name")
                            UserDefaults.standard.set(self.surname.text!, forKey: "surname")
                            UserDefaults.standard.set(uid, forKey: "uid")
                            self.performSegue(withIdentifier: "nextStep", sender: self)
                        }
                        
                    }
                }
                
            }
            
        }
        
    }
    
    @IBAction func logIn(_ sender: Any) {
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    
}
