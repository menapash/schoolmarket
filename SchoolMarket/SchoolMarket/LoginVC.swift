//
//  LoginVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 10/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginVC: UIViewController, GIDSignInUIDelegate, UITextFieldDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var passwordForgotten: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    
    var indicator: ProgressIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        /*indicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "Login...")
        self.view.addSubview(self.indicator!)
        indicator?.isHidden = true*/
        
        email.delegate = self
        email.tag = 0
        password.delegate = self
        password.tag = 1

        loginButton.layer.cornerRadius = 5
        
        googleButton.setTitle("Google", for: .normal)
        googleButton.setTitleColor(UIColor.black, for: .normal)
        googleButton.setImage(UIImage(named: "logo_google"), for: .normal)
        googleButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 40)
        googleButton.layer.cornerRadius = 5
        googleButton.layer.borderWidth = 1
        googleButton.layer.borderColor = UIColor.lightGray.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginVC.tapFunction))
        passwordForgotten.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginBC(_ sender: Any) {
        if email.text!.isEmpty || password.text!.isEmpty {
            
            //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
            
            let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil);
            
            //Coloro i placeholders di rosso
            email.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            
            //Se tutti i campi obbligatori sono stati inseriti, proseguo ad altri controlli
        }else{
            Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (user, error) in
                // ...
                if let user = user {
                    let uid = user.uid as! String
                    let email = user.email
                    // ...
                    let db = Firestore.firestore()
                    let docRef = db.collection("users").document(uid)
                    
                    docRef.getDocument { (document, error) in
                        if let document = document {
                            let data = document.data()
                            UserDefaults.standard.set(data!["email"] as! String, forKey: "userEmail")
                            UserDefaults.standard.set(data!["name"] as! String, forKey: "userName")
                            UserDefaults.standard.set(data!["surname"] as! String, forKey: "userSurname")
                            UserDefaults.standard.set(data!["city"] as! String, forKey: "userCity")
                            UserDefaults.standard.set(data!["lat"] as! String, forKey: "userLatitude")
                            UserDefaults.standard.set(data!["lng"] as! String, forKey: "userLongitude")
                            if(data!["isStudying"] as! Bool == true){
                                UserDefaults.standard.set(data!["isStudying"] as! Bool, forKey: "isStudying")
                                UserDefaults.standard.set(data!["student"] as! Bool, forKey: "student")
                                UserDefaults.standard.set(data!["specialization"] as! String, forKey: "userSpecialization")
                                UserDefaults.standard.set(data!["schoolLat"] as! String, forKey: "userSchoolLatitude")
                                UserDefaults.standard.set(data!["schoolLng"] as! String, forKey: "userSchoolLongitude")
                                if(data!["student"] as! Bool == false){
                                    UserDefaults.standard.set(data!["school"] as! String, forKey: "userUniversity")
                                } else {
                                    UserDefaults.standard.set(data!["school"] as! String, forKey: "userSchool")
                                }
                                
                            }
                            UserDefaults.standard.set(uid, forKey: "userUid")
                            self.performSegue(withIdentifier: "toHome", sender: self)
                        } else {
                            print("Document does not exist")
                        }
                    }
                    
                } else {
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        
                        switch errCode {
                        case .invalidEmail:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "Inserisci un indirizzo e-mail valido", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                            
                        case .wrongPassword:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "E-mail o password errata", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                            
                        case .userNotFound:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "L'indirizzo e-mail non corrisponde ad un account", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                            
                        default:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "Errore nella registrazione", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func googleBC(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let alertController = UIAlertController(title: "Inserisci l'indirizzo e-mail", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Invia mail", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let textField = alertController.textFields![0] as UITextField
            
            Auth.auth().sendPasswordReset(withEmail: textField.text!) { (error) in
                if (error == nil) {
                    let myAlert = UIAlertController(title: "Email inviata \n", message: "Se non visualizzi la mail nella posta in arrivo, controlla nella posta indesiderata/spam", preferredStyle: UIAlertControllerStyle.alert);
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                    myAlert.addAction(okAction);
                    self.present(myAlert, animated: true, completion: nil);
                } else {
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        
                        switch errCode {
                        case .invalidEmail:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "Inserisci un indirizzo e-mail valido", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                            
                        case .userNotFound:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "L'indirizzo e-mail non corrisponde ad un account", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                            
                        default:
                            let myAlert = UIAlertController(title: "Attenzione\n", message: "Errore nella registrazione", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                            break
                        }
                    }
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Esci", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "E-mail"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func signInBC(_ sender: Any) {
        self.performSegue(withIdentifier: "toRegistration", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
}
