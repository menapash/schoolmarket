//
//  Methods.swift
//  SchoolMarket
//
//  Created by Marco PC on 22/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import Foundation
import UIKit

class Methods{
    
    static func styleTextField(textField: UITextField, color: UIColor){
        textField.layer.borderColor = color.cgColor
        textField.layer.borderWidth = 0.5
        textField.layer.cornerRadius = 5
        /*textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 3.0
        textField.layer.shadowColor = UIColor.gray.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0*/
    }
    
    static func styleLabel(label: UILabel, bColor: UIColor, tColor: UIColor){
        label.textColor = tColor
        label.layer.borderColor = bColor.cgColor
        label.layer.borderWidth = 0.5
        label.layer.cornerRadius = 5
    }
    
    static func styleButton(button: UIButton){
        button.layer.masksToBounds = false
        button.layer.shadowRadius = 3.0
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        button.layer.shadowOpacity = 1.0
    }
    
}
