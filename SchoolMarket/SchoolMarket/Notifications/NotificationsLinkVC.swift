//
//  NotificationsVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 09/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class NotificationsLinkVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Notifications", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! UIViewController
        addChildViewController(vc)
        view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }

}
