//
//  ChangeProfileTableViewCell.swift
//  SchoolMarket
//
//  Created by Marco PC on 29/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class ChangeProfileTableViewCell: UITableViewCell {

    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var profileTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
