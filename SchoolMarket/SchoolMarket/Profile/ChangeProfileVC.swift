//
//  ChangeProfileVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 29/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class ChangeProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    //var labels = ["Nome","Cognome","Email","Comune di residenza","Comune di (?)","Istituto","Facoltà"]
    var labels = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.bool(forKey: "isStudying")," - ",UserDefaults.standard.bool(forKey: "student"))
        if(UserDefaults.standard.bool(forKey: "isStudying")){
            if(UserDefaults.standard.bool(forKey: "student")){
                self.labels = ["Nome","Cognome","Email","Comune di residenza","Istituto","Specializzazione"]
            } else {
                self.labels = ["Nome","Cognome","Email","Comune di residenza","Università","Facoltà"]
            }
            
        } else {
            self.labels = ["Nome","Cognome","Email","Comune di residenza"]
        }
        
        profileImageView.layer.cornerRadius = 75
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return labels.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChangeProfileTableViewCell

        
        cell.descriptionLabel.text = labels[indexPath.section]
        
        if(UserDefaults.standard.bool(forKey: "isStudying")){
            switch(indexPath.section){
            case 0:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userName")
                break
            case 1:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userSurname")
                break
            case 2:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userEmail")
                break
            case 3:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userCity")
                break
            case 4:
                if(UserDefaults.standard.bool(forKey: "student")){
                    cell.profileTextField.text = UserDefaults.standard.string(forKey: "userSchool")
                } else {
                    cell.profileTextField.text = UserDefaults.standard.string(forKey: "userUniversity")
                }
                break
            case 5:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userSpecialization")
                break
            default:
                break
            }
        } else {
            switch(indexPath.section){
            case 0:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userName")
                break
            case 1:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userSurname")
                break
            case 2:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userEmail")
                break
            case 3:
                cell.profileTextField.text = UserDefaults.standard.string(forKey: "userCity")
                break
            default:
                break
            }
        }
        
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UserDefaults.standard.set(indexPath.section, forKey: "rowSelected")
        print("riga",UserDefaults.standard.integer(forKey: "rowSelected"))
        
        self.performSegue(withIdentifier: "tasksListToModifyTask", sender: self)
        
    }*/

}
