//
//  ProfileVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 25/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase
import Photos

class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var email: UILabel!
    
    var indicator:ProgressIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "Salvataggio foto...")
        self.view.addSubview(self.indicator!)

        username.text = UserDefaults.standard.string(forKey: "userName")! + " " + UserDefaults.standard.string(forKey: "userSurname")!
        email.text = UserDefaults.standard.string(forKey: "userEmail")
        
        if(UserDefaults.standard.string(forKey: "userPhotoUrl") != nil){
            let url = URL(string: UserDefaults.standard.string(forKey: "userPhotoUrl")!)
            print(url)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            profileImageView.image = UIImage(data: data!)
        }
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        profileImageView.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }

    @IBAction func SettingsBC(_ sender: Any) {
        self.performSegue(withIdentifier: "toSettings", sender: self)
    }
    
    @objc private func tapImage(){
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        let actionSheet = UIAlertController(title: "Foto profilo", message: "Scegli una foto", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else{
                print("Camera non disponibile")
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            switch photoAuthorizationStatus {
            case .authorized:
                print("Access is granted by user")
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    imagePickerController.sourceType = .photoLibrary
                    self.present(imagePickerController, animated: true, completion: nil)
                } else{
                    print("Galleria non disponibile")
                }
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({
                    (newStatus) in
                    print("status is \(newStatus)")
                    if newStatus ==  PHAuthorizationStatus.authorized {
                        /* do stuff here */
                        print("success")
                        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                            imagePickerController.sourceType = .photoLibrary
                            self.present(imagePickerController, animated: true, completion: nil)
                        } else{
                            print("Galleria non disponibile")
                        }
                    }
                })
                print("It is not determined until now")
            case .restricted:
                // same same
                print("User do not have access to photo album.")
            case .denied:
                // same same
                print("User has denied the permission.")
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancella", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var image: UIImage?
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = originalImage
        }
        
        // File located on disk
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let referenceUrl = info[UIImagePickerControllerReferenceURL] as! NSURL
        let assets = PHAsset.fetchAssets(withALAssetURLs: [referenceUrl as URL], options: nil)
        let asset = assets.firstObject
        asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
            let imageFile = contentEditingInput?.fullSizeImageURL
            print("ImageFile: \n",imageFile!)
            let riversRef = storageRef.child("profilePic/"+UserDefaults.standard.string(forKey: "userUid")!)
            
            self.indicator?.isHidden = false
            self.indicator!.start()
            
            //Upload as image
            /*let uploadTask = riversRef.putFile(from: imageFile!, metadata: nil, completion: { (metadata, error) in
             if let error = error {
             // Uh-oh, an error occurred!
             print("Error: ",error)
             self.indicator!.stop()
             self.indicator?.isHidden = true
             } else {
             // Metadata contains file metadata such as size, content-type, and download URL.
             print("Success")
             self.indicator!.stop()
             self.indicator?.isHidden = true
             //let downloadURL = metadata!.downloadURL()
             }
             })*/
            
            //Upload as data
            let upload = riversRef.putData(try! Data(contentsOf: imageFile!), metadata: nil){ metadata, error in
                if let error = error {
                    // Uh-oh, an error occurred!
                    print("Error: ",error)
                    self.indicator!.stop()
                    self.indicator?.isHidden = true
                } else {
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    print("Success")
                    self.indicator!.stop()
                    self.indicator?.isHidden = true
                    //let downloadURL = metadata!.downloadURL()
                }
            }
        })
        
        profileImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
