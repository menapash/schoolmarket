//
//  SettingsVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 25/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var settingsTableView: UITableView!
    
    var titolo = ["Modifica il tuo profilo", "Notifiche","Lascia un feedback","Invita i tuoi amici","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titolo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        tableView.rowHeight = 70
        switch(indexPath.row){
        case 0:
            /*let imageName = "Logo_ArancioBluGrigio-3"
            let image = UIImage(named: imageName)
            cell.imageView!.image = image*/
            break
        case 1:
            /*let imageName = "app_store-3"
            let image = UIImage(named: imageName)
            cell.imageView!.image = image*/
            break
        case 2:
            /*let imageName = "facebook-3"
            let image = UIImage(named: imageName)
            cell.imageView!.image = image*/
            break
        case 3:
            /*let imageName = "instagram_2-3"
            let image = UIImage(named: imageName)
            cell.imageView!.image = image*/
            break
        case 4:
            /*let imageName = "website-3"
            let image = UIImage(named: imageName)
            cell.imageView!.image = image*/
            break
        default:
            break
        }
        
        cell.textLabel?.text = titolo[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.row){
        case 0:
            self.performSegue(withIdentifier: "toChangeProfile", sender: self)
            break
        case 1:
            /*let imageName = "app_store-3"
             let image = UIImage(named: imageName)
             cell.imageView!.image = image*/
            break
        case 2:
            /*let url  = NSURL(string: "http://itunes.apple.com/app/id1240880116")
            if UIApplication.shared.canOpenURL(url! as URL) {
                UIApplication.shared.openURL(url! as URL)
            }*/
            break
        case 3:
            let activityVC = UIActivityViewController(activityItems: ["Hey, installa SchoolMarket per facilitare la tua organizzazione scolastica\nAppStore = \n\nPlayStore: "], applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.view
            
            self.present(activityVC, animated: true, completion: nil)
            break
        case 4:
            /*let imageName = "website-3"
             let image = UIImage(named: imageName)
             cell.imageView!.image = image*/
            break
        default:
            break
        }
        
        /*switch(indexPath.row){
        case 1:
            let url  = NSURL(string: "http://itunes.apple.com/app/id1240880116")
            if UIApplication.shared.canOpenURL(url! as URL) {
                UIApplication.shared.openURL(url! as URL)
            }
            break
        case 2:
            if let url = URL(string: "fb://profile/763082880540775") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:],completionHandler: { (success) in
                        if !success{
                            UIApplication.shared.openURL(URL(string: "https://m.facebook.com/aesolutions.256/")!)
                        }
                    })
                } else {
                    UIApplication.shared.openURL(URL(string: "https://m.facebook.com/aesolutions.256/")!)
                }
            }
            break
        case 3:
            let Username =  "aesolutions.256"
            if let url = URL(string: "instagram://user?username=\(Username)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:],completionHandler: { (success) in
                        if !success{
                            UIApplication.shared.openURL(URL(string: "https://www.instagram.com/aesolutions.256/")!)
                        }
                    })
                } else {
                    UIApplication.shared.openURL(URL(string: "https://www.instagram.com/aesolutions.256/")!)
                }
            }
            break
        case 4:
            UIApplication.shared.openURL(URL(string: "http://aesolutions.ddns.net/schooltools/")!)
            break
        case 5:
            
            
            let myAlert = UIAlertController(title: "Come vuoi contattarci ?\n", message:nil, preferredStyle: UIAlertControllerStyle.alert);
            
            let mailAction = UIAlertAction(title: "Mail", style: UIAlertActionStyle.default){ action in
                
                let url = NSURL(string: "mailto:aesolutions.256@gmail.com")
                UIApplication.shared.openURL(url! as URL)
            }
            
            let chatAction = UIAlertAction(title: "Chat", style: UIAlertActionStyle.default){ action in
                
                UserDefaults.standard.set("56", forKey: "chatID")
                
                self.performSegue(withIdentifier: "chatBug", sender: self)
                
                
                /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let viewController = storyboard.instantiateViewController(withIdentifier :"chatList") as! ChatViewController
                 self.present(viewController, animated: true)*/
                
            }
            
            myAlert.addAction(mailAction);
            myAlert.addAction(chatAction);
            
            self.present(myAlert, animated: true, completion: nil);
            
            /*let url = NSURL(string: "mailto:aesolutions.256@gmail.com")
             UIApplication.shared.openURL(url! as URL)*/
            break
        case 6:
            let activityVC = UIActivityViewController(activityItems: ["Hey, installa SchoolTools per facilitare la tua organizzazione scolastica\nAppStore = http://itunes.apple.com/app/id1240880116\n\nPlayStore: https://play.google.com/store/apps/details?id=com.aesolutions.schooltools"], applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.view
            
            self.present(activityVC, animated: true, completion: nil)
            break
            /*case 7:
             //UIApplication.shared.openURL(URL(string: "http://aesolutions.ddns.net/schooltools/donate/")!)
             if ad.interstitialVideo.isReady {
             ad.interstitialVideo.present(fromRootViewController: self)
             }
             else {
             print("Ad is not ready")
             }
             
             
             
             break*/
        case 7:
            let alertController = UIAlertController(title: "Inserisci la fine del quadrimestre", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Salva", style: UIAlertActionStyle.default, handler: {
                alert -> Void in
                
                let textField = alertController.textFields![0] as UITextField
                
                UserDefaults.standard.set(textField.text!, forKey: "period")
                tableView.reloadData()
                
            })
            
            let cancelAction = UIAlertAction(title: "Esci", style: UIAlertActionStyle.default, handler: {
                (action : UIAlertAction!) -> Void in })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "gg/mm/aaaa"
            }
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            break
        default:
            break
        }*/
    }

}
