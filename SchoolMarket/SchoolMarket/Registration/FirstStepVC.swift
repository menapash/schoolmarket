//
//  FirstStepVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 17/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class FirstStepVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\n\nviewDidLoad",UserDefaults.standard.bool(forKey: "googleBC"),"\n\n")
        
        Methods.styleTextField(textField: name, color: UIColor.lightGray)
        Methods.styleTextField(textField: surname, color: UIColor.lightGray)
        
        name.delegate = self
        name.tag = 0
        surname.delegate = self
        surname.tag = 1
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("\n\nviewWillAppear",UserDefaults.standard.bool(forKey: "googleBC"),"\n\n")
        if(UserDefaults.standard.bool(forKey: "googleBC")){
            name.text = UserDefaults.standard.string(forKey: "gUserName")
        }
        
        if(UserDefaults.standard.string(forKey: "name") != nil){
            name.text = UserDefaults.standard.string(forKey: "name")!
        }
        
        if(UserDefaults.standard.string(forKey: "surname") != nil){
            surname.text = UserDefaults.standard.string(forKey: "surname")!
        }
    }

    @IBAction func nextStepBC(_ sender: Any) {
        if name.text!.isEmpty || surname.text!.isEmpty {
            
            //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
            
            let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil);
            
            if name.text!.isEmpty{
                name.attributedPlaceholder = NSAttributedString(string: "Nome", attributes: nil)
                Methods.styleTextField(textField: name, color: UIColor.red)
            }
            
            if surname.text!.isEmpty{
                surname.attributedPlaceholder = NSAttributedString(string: "Cognome", attributes: nil)
                Methods.styleTextField(textField: surname, color: UIColor.lightGray)
            }
            
        }else{
            UserDefaults.standard.set(name.text!, forKey: "name")
            UserDefaults.standard.set(surname.text!, forKey: "surname")
            self.performSegue(withIdentifier: "toSecondStep", sender: self)
        }
    }
    
    @IBAction func isEditingName(_ sender: Any) {
        Methods.styleTextField(textField: name, color: UIColor.lightGray)
    }
    
    @IBAction func isEditingSurname(_ sender: Any) {
        Methods.styleTextField(textField: surname, color: UIColor.lightGray)
    }
    
    @IBAction func toLoginBC(_ sender: Any) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}
