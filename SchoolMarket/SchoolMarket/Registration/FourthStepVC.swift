//
//  FourthStepVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 17/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase

class FourthStepVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(UserDefaults.standard.bool(forKey: "googleBC")){
            email.text = UserDefaults.standard.string(forKey: "gUserEmail")
            email.isUserInteractionEnabled = false
            password.isHidden = true
            repeatPassword.isHidden = true
        }
        
        Methods.styleTextField(textField: email, color: UIColor.lightGray)
        Methods.styleTextField(textField: password, color: UIColor.lightGray)
        Methods.styleTextField(textField: repeatPassword, color: UIColor.lightGray)
        
        email.delegate = self
        email.tag = 0
        password.delegate = self
        password.tag = 1
        repeatPassword.delegate = self
        repeatPassword.tag = 2
        // Do any additional setup after loading the view.
    }

    @IBAction func previousStepBC(_ sender: Any) {
        if(UserDefaults.standard.bool(forKey: "isStudying")){
            self.performSegue(withIdentifier: "toThirdStep", sender: self)
        } else {
            self.performSegue(withIdentifier: "fromFourthToSecondStep", sender: self)
        }
    }
    
    @IBAction func registerBC(_ sender: Any) {
        if(UserDefaults.standard.bool(forKey: "googleBC")){
            if(!UserDefaults.standard.bool(forKey: "isStudying")){
                self.db.collection("users").document(UserDefaults.standard.string(forKey: "gUserUid")!).setData([
                    "name": UserDefaults.standard.string(forKey: "name")!,
                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                    "isStudying": false,
                    "city": UserDefaults.standard.string(forKey: "city")!,
                    "lat": UserDefaults.standard.string(forKey: "latitude")!,
                    "lng": UserDefaults.standard.string(forKey: "longitude")!,
                    "email": self.email.text!
                ], options: .merge()) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                            } catch let signOutError as NSError {
                                print ("Error signing out: ", signOutError)
                            }
                        }
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    }
                }
            } else {
                if(UserDefaults.standard.bool(forKey: "student")){
                    self.db.collection("users").document(UserDefaults.standard.string(forKey: "gUserUid")!).setData([
                        "name": UserDefaults.standard.string(forKey: "name")!,
                        "surname": UserDefaults.standard.string(forKey: "surname")!,
                        "isStudying": true,
                        "student": true,
                        "city": UserDefaults.standard.string(forKey: "city")!,
                        "lat": UserDefaults.standard.string(forKey: "latitude"),
                        "lng": UserDefaults.standard.string(forKey: "longitude"),
                        "school": UserDefaults.standard.string(forKey: "school")!,
                        "schoolLat": UserDefaults.standard.string(forKey: "schoolLatitude")!,
                        "schoolLng": UserDefaults.standard.string(forKey: "schoolLongitude")!,
                        "specialization": UserDefaults.standard.string(forKey: "specialization")!,
                        "email": self.email.text!
                    ], options: .merge()) { err in
                        if let err = err {
                            print("Error writing document: \(err)")
                        } else {
                            let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                                let firebaseAuth = Auth.auth()
                                do {
                                    try firebaseAuth.signOut()
                                } catch let signOutError as NSError {
                                    print ("Error signing out: ", signOutError)
                                }
                            }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                        }
                    }
                } else {
                    self.db.collection("users").document(UserDefaults.standard.string(forKey: "gUserUid")!).setData([
                        "name": UserDefaults.standard.string(forKey: "name")!,
                        "surname": UserDefaults.standard.string(forKey: "surname")!,
                        "isStudying": true,
                        "student": false,
                        "city": UserDefaults.standard.string(forKey: "city")!,
                        "lat": UserDefaults.standard.string(forKey: "latitude")!,
                        "lng": UserDefaults.standard.string(forKey: "longitude")!,
                        "school": UserDefaults.standard.string(forKey: "university")!,
                        "schoolLat": UserDefaults.standard.string(forKey: "schoolLatitude")!,
                        "schoolLng": UserDefaults.standard.string(forKey: "schoolLongitude")!,
                        "specialization": UserDefaults.standard.string(forKey: "specialization")!,
                        "email": self.email.text!
                    ], options: .merge()) { err in
                        if let err = err {
                            print("Error writing document: \(err)")
                        } else {
                            let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                                let firebaseAuth = Auth.auth()
                                do {
                                    try firebaseAuth.signOut()
                                } catch let signOutError as NSError {
                                    print ("Error signing out: ", signOutError)
                                }
                                self.performSegue(withIdentifier: "toLogin", sender: self)
                            }
                            myAlert.addAction(okAction);
                            self.present(myAlert, animated: true, completion: nil);
                        }
                    }
                }
            }
        } else {
            if email.text!.isEmpty || password.text!.isEmpty || repeatPassword.text!.isEmpty {
                
                //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
                
                let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                myAlert.addAction(okAction);
                self.present(myAlert, animated: true, completion: nil);
                
                if email.text!.isEmpty{
                    Methods.styleTextField(textField: email, color: UIColor.red)
                }
                if password.text!.isEmpty{
                    Methods.styleTextField(textField: password, color: UIColor.red)
                }
                if repeatPassword.text!.isEmpty{
                    Methods.styleTextField(textField: repeatPassword, color: UIColor.red)
                }
                
            }else{
                if(password.text! != repeatPassword.text!){
                    
                    //Alert per segnalare le due password diverse
                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Le due password devono coincidere", preferredStyle:UIAlertControllerStyle.alert);
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                    myAlert.addAction(okAction);
                    self.present(myAlert, animated: true, completion: nil);
                    
                }else{
                    Auth.auth().createUser(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
                        let uid = user?.uid
                        if error != nil {
                            
                            if let errCode = AuthErrorCode(rawValue: error!._code) {
                                
                                switch errCode {
                                case .invalidEmail:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Inserisci un indirizzo e-mail valido", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                case .emailAlreadyInUse:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Indirizzo e-mail già in uso", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                case .weakPassword:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "La password deve essere almeno di 6 caratteri", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                    
                                default:
                                    let myAlert = UIAlertController(title: "Attenzione\n", message: "Errore nella registrazione", preferredStyle: UIAlertControllerStyle.alert);
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
                                    myAlert.addAction(okAction);
                                    self.present(myAlert, animated: true, completion: nil);
                                    break
                                }
                            }
                            
                        } else{
                            if(!UserDefaults.standard.bool(forKey: "isStudying")){
                                self.db.collection("users").document(uid!).setData([
                                    "name": UserDefaults.standard.string(forKey: "name")!,
                                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                                    "isStudying": false,
                                    "city": UserDefaults.standard.string(forKey: "city")!,
                                    "lat": UserDefaults.standard.string(forKey: "latitude")!,
                                    "lng": UserDefaults.standard.string(forKey: "longitude")!,
                                    "email": self.email.text!
                                ], options: .merge()) { err in
                                    if let err = err {
                                        print("Error writing document: \(err)")
                                    } else {
                                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                                            let domain = Bundle.main.bundleIdentifier!
                                            UserDefaults.standard.removePersistentDomain(forName: domain)
                                            UserDefaults.standard.synchronize()
                                            let firebaseAuth = Auth.auth()
                                            do {
                                                try firebaseAuth.signOut()
                                            } catch let signOutError as NSError {
                                                print ("Error signing out: ", signOutError)
                                            }
                                        }
                                        myAlert.addAction(okAction);
                                        self.present(myAlert, animated: true, completion: nil);
                                    }
                                }
                            } else {
                                if(UserDefaults.standard.bool(forKey: "student")){
                                    self.db.collection("users").document(uid!).setData([
                                        "name": UserDefaults.standard.string(forKey: "name")!,
                                        "surname": UserDefaults.standard.string(forKey: "surname")!,
                                        "isStudying": true,
                                        "student": true,
                                        "city": UserDefaults.standard.string(forKey: "city")!,
                                        "lat": UserDefaults.standard.string(forKey: "latitude"),
                                        "lng": UserDefaults.standard.string(forKey: "longitude"),
                                        "school": UserDefaults.standard.string(forKey: "school")!,
                                        "schoolLat": UserDefaults.standard.string(forKey: "schoolLatitude")!,
                                        "schoolLng": UserDefaults.standard.string(forKey: "schoolLongitude")!,
                                        "specialization": UserDefaults.standard.string(forKey: "specialization")!,
                                        "email": self.email.text!
                                    ], options: .merge()) { err in
                                        if let err = err {
                                            print("Error writing document: \(err)")
                                        } else {
                                            let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                                                let domain = Bundle.main.bundleIdentifier!
                                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                                UserDefaults.standard.synchronize()
                                                let firebaseAuth = Auth.auth()
                                                do {
                                                    try firebaseAuth.signOut()
                                                } catch let signOutError as NSError {
                                                    print ("Error signing out: ", signOutError)
                                                }
                                            }
                                            myAlert.addAction(okAction);
                                            self.present(myAlert, animated: true, completion: nil);
                                        }
                                    }
                                } else {
                                    self.db.collection("users").document(uid!).setData([
                                        "name": UserDefaults.standard.string(forKey: "name")!,
                                        "surname": UserDefaults.standard.string(forKey: "surname")!,
                                        "isStudying": true,
                                        "student": false,
                                        "city": UserDefaults.standard.string(forKey: "city")!,
                                        "lat": UserDefaults.standard.string(forKey: "latitude")!,
                                        "lng": UserDefaults.standard.string(forKey: "longitude")!,
                                        "school": UserDefaults.standard.string(forKey: "university")!,
                                        "schoolLat": UserDefaults.standard.string(forKey: "schoolLatitude")!,
                                        "schoolLng": UserDefaults.standard.string(forKey: "schoolLongitude")!,
                                        "specialization": UserDefaults.standard.string(forKey: "specialization")!,
                                        "email": self.email.text!
                                    ], options: .merge()) { err in
                                        if let err = err {
                                            print("Error writing document: \(err)")
                                        } else {
                                            let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                                                let domain = Bundle.main.bundleIdentifier!
                                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                                UserDefaults.standard.synchronize()
                                                let firebaseAuth = Auth.auth()
                                                do {
                                                    try firebaseAuth.signOut()
                                                } catch let signOutError as NSError {
                                                    print ("Error signing out: ", signOutError)
                                                }
                                            }
                                            myAlert.addAction(okAction);
                                            self.present(myAlert, animated: true, completion: nil);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func isEditingEmail(_ sender: Any) {
        Methods.styleTextField(textField: email, color: UIColor.lightGray)
    }
    
    @IBAction func isEditingPassword(_ sender: Any) {
        Methods.styleTextField(textField: password, color: UIColor.lightGray)
    }
    
    
    @IBAction func isEditingRepeatPassword(_ sender: Any) {
        Methods.styleTextField(textField: repeatPassword, color: UIColor.lightGray)
    }
    
    @IBAction func toLoginBC(_ sender: Any) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}
