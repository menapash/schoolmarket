//
//  SecondStepVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 17/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlaces

class SecondStepVC: UIViewController {

    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var mySwitch: UISwitch!
    
    var resultText: UITextView?
    var fetcher: GMSAutocompleteFetcher?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Methods.styleLabel(label: city, bColor: UIColor.lightGray, tColor: UIColor.lightGray)
        city.text = "  Città"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        city.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UserDefaults.standard.string(forKey: "city") != nil){
            Methods.styleLabel(label: city, bColor: UIColor.lightGray, tColor: UIColor.black)
            city.text = "  " + UserDefaults.standard.string(forKey: "city")!
        }
        if(UserDefaults.standard.bool(forKey: "isStudying")){
            mySwitch.isOn = true
        }
    }

    @IBAction func previousStepBC(_ sender: Any) {
        self.performSegue(withIdentifier: "toFirstStep", sender: self)
    }
    
    @IBAction func nextStepBC(_ sender: Any) {
        if city.text! == "  Città" {
            
            //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
            
            let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil);
            
            city.text = "  Città"
            Methods.styleLabel(label: city, bColor: UIColor.red, tColor: UIColor.lightGray)
            
        }else{
            if mySwitch.isOn {
                UserDefaults.standard.set(true, forKey: "isStudying")
                self.performSegue(withIdentifier: "toThirdStep", sender: self)
            } else {
                UserDefaults.standard.set(false, forKey: "isStudying")
                self.performSegue(withIdentifier: "fromSecondToFourthStep", sender: self)
            }
        }
    }
}

extension SecondStepVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.coordinate)")
        print("Province: \(place.addressComponents)")
        UserDefaults.standard.set(String(describing: place.coordinate.latitude), forKey: "latitude")
        UserDefaults.standard.set(String(describing: place.coordinate.longitude), forKey: "longitude")
        UserDefaults.standard.set(place.name, forKey: "city")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
