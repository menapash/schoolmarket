//
//  ThirdStepVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 17/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlaces

class ThirdStepVC: UIViewController {

    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var school: UILabel!
    @IBOutlet weak var specialization: UITextField!
    
    var resultText: UITextView?
    var fetcher: GMSAutocompleteFetcher?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set(true, forKey: "student")
        Methods.styleLabel(label: school, bColor: UIColor.lightGray, tColor: UIColor.lightGray)
        school.text = "  Scuola"
        
        Methods.styleTextField(textField: specialization, color: UIColor.lightGray)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        school.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UserDefaults.standard.bool(forKey: "student")){
            if(UserDefaults.standard.string(forKey: "school") != nil){
                Methods.styleLabel(label: school, bColor: UIColor.lightGray, tColor: UIColor.black)
                school.text = "  " + UserDefaults.standard.string(forKey: "school")!
            }
        } else {
            mySwitch.isOn = true
            if(UserDefaults.standard.string(forKey: "university") != nil){
                Methods.styleLabel(label: school, bColor: UIColor.lightGray, tColor: UIColor.black)
                school.text = "  " + UserDefaults.standard.string(forKey: "university")!
            }
        }
    }

    @IBAction func switchBC(_ sender: Any) {
        if(self.mySwitch.isOn) {
            UserDefaults.standard.set(false, forKey: "student")
            Methods.styleLabel(label: school, bColor: UIColor.lightGray, tColor: UIColor.lightGray)
            school.text = "  Università"
            specialization.attributedPlaceholder = NSAttributedString(string: "Facoltà", attributes: nil)
        } else {
            UserDefaults.standard.set(true, forKey: "student")
            Methods.styleLabel(label: school, bColor: UIColor.lightGray, tColor: UIColor.lightGray)
            school.text = "  Scuola"
            specialization.attributedPlaceholder = NSAttributedString(string: "Specializzazione", attributes: nil)
        }
    }
    
    @IBAction func previousStepBC(_ sender: Any) {
        self.performSegue(withIdentifier: "toSecondStep", sender: self)
    }
    
    @IBAction func nextStepBC(_ sender: Any) {
        if school.text!.isEmpty || specialization.text!.isEmpty {
            
            //Alert per segnalare i campi obbligatori mancanti, oltre a caselle rosse
            
            let myAlert = UIAlertController(title: "Attenzione\n", message: "Campi obbligatori mancanti", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil);
            
            if school.text!.isEmpty{
                if(self.mySwitch.isOn) {
                    school.text = "  Università"
                } else {
                    school.text = "  Scuola"
                }
                Methods.styleLabel(label: school, bColor: UIColor.red, tColor: UIColor.lightGray)
            }
            
            if specialization.text!.isEmpty{
                if(self.mySwitch.isOn) {
                    specialization.attributedPlaceholder = NSAttributedString(string: "Facoltà", attributes: nil)
                } else {
                    specialization.attributedPlaceholder = NSAttributedString(string: "Specializzazione", attributes: nil)
                }
                Methods.styleTextField(textField: specialization, color: UIColor.red)
            }
            
        }else{
            UserDefaults.standard.set(specialization.text!, forKey: "specialization")
            self.performSegue(withIdentifier: "toFourthStep", sender: self)
        }
    }
    
    @IBAction func isEditingSpecialization(_ sender: Any) {
        Methods.styleTextField(textField: specialization, color: UIColor.lightGray)
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
}

extension ThirdStepVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.coordinate)")
        print("Province: \(place.addressComponents)")
        UserDefaults.standard.set(String(describing: place.coordinate.latitude), forKey: "schoolLatitude")
        UserDefaults.standard.set(String(describing: place.coordinate.longitude), forKey: "schoolLongitude")
        if(UserDefaults.standard.bool(forKey: "student")){
            UserDefaults.standard.set(place.name, forKey: "school")
        } else {
            UserDefaults.standard.set(place.name, forKey: "university")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
