//
//  SecondStepRegistrationVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 10/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MapKit
import GooglePlaces

class SecondStepRegistrationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var university: UITextField!
    @IBOutlet weak var faculty: UITextField!
    @IBOutlet weak var mySwitch: UISwitch!
    
    var ref: DocumentReference? = nil
    let db = Firestore.firestore()
    
    var resultText: UITextView?
    var fetcher: GMSAutocompleteFetcher?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        city.textColor = UIColor.lightGray
        city.text = "Città"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        city.addGestureRecognizer(tap)
        
        university.delegate = self
        university.tag = 1
        faculty.delegate = self
        faculty.tag = 2
        
        registerButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UserDefaults.standard.string(forKey: "city") != nil){
            city.textColor = UIColor.black
            city.text = UserDefaults.standard.string(forKey: "city")!
        }
    }

    @IBAction func switchBC(_ sender: Any) {
        if(self.mySwitch.isOn) {
            university.isHidden = false
            faculty.isHidden = false
        } else {
            university.isHidden = true
            faculty.isHidden = true
        }
    }
    
    @IBAction func registerBC(_ sender: Any) {
        if(UserDefaults.standard.bool(forKey: "googleBC")){
            if(mySwitch.isOn){
                self.db.collection("users").document(UserDefaults.standard.string(forKey: "gUserUid")!).setData([
                    "name": UserDefaults.standard.string(forKey: "name")!,
                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                    "email": UserDefaults.standard.string(forKey: "gUserEmail")!,
                    "city": city.text!,
                    "student": false,
                    "lat": UserDefaults.standard.string(forKey: "latitude"),
                    "lng": UserDefaults.standard.string(forKey: "longitude"),
                    "university": university.text!,
                    "faculty": faculty.text!
                ], options: .merge()) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                            } catch let signOutError as NSError {
                                print ("Error signing out: ", signOutError)
                            }
                        }
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    }
                }
            } else {
                self.db.collection("users").document(UserDefaults.standard.string(forKey: "gUserUid")!).setData([
                    "name": UserDefaults.standard.string(forKey: "name")!,
                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                    "email": UserDefaults.standard.string(forKey: "gUserEmail")!,
                    "city": city.text!,
                    "lat": UserDefaults.standard.string(forKey: "latitude"),
                    "lng": UserDefaults.standard.string(forKey: "longitude"),
                    "student": true
                ], options: .merge()) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                            } catch let signOutError as NSError {
                                print ("Error signing out: ", signOutError)
                            }
                        }
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    }
                }
            }
        } else {
            if(mySwitch.isOn){
                self.db.collection("users").document(UserDefaults.standard.string(forKey: "uid")!).setData([
                    "name": UserDefaults.standard.string(forKey: "name")!,
                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                    "email": UserDefaults.standard.string(forKey: "email")!,
                    "city": city.text!,
                    "student": false,
                    "lat": UserDefaults.standard.string(forKey: "latitude"),
                    "lng": UserDefaults.standard.string(forKey: "longitude"),
                    "university": university.text!,
                    "faculty": faculty.text!
                ], options: .merge()) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                            } catch let signOutError as NSError {
                                print ("Error signing out: ", signOutError)
                            }
                        }
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    }
                }
            } else {
                self.db.collection("users").document(UserDefaults.standard.string(forKey: "uid")!).setData([
                    "name": UserDefaults.standard.string(forKey: "name")!,
                    "surname": UserDefaults.standard.string(forKey: "surname")!,
                    "email": UserDefaults.standard.string(forKey: "email")!,
                    "city": city.text!,
                    "lat": UserDefaults.standard.string(forKey: "latitude"),
                    "lng": UserDefaults.standard.string(forKey: "longitude"),
                    "student": true
                ], options: .merge()) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        let myAlert = UIAlertController(title: "Utente aggiunto", message: nil, preferredStyle: UIAlertControllerStyle.alert);
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                            } catch let signOutError as NSError {
                                print ("Error signing out: ", signOutError)
                            }
                        }
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension SecondStepRegistrationVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.coordinate)")
        print("Province: \(place.addressComponents)")
        UserDefaults.standard.set(String(describing: place.coordinate.latitude), forKey: "latitude")
        UserDefaults.standard.set(String(describing: place.coordinate.longitude), forKey: "longitude")
        UserDefaults.standard.set(place.name, forKey: "city")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
