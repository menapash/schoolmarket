//
//  BookSellingFirstVC.swift
//  SchoolMarket
//
//  Created by Edward Ducklain on 06/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class BookSellingFirstVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var ISBNText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ISBNText.text = ScannerViewController.ISBNCode
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ISBNText.text = ScannerViewController.ISBNCode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func QRScanner(_ sender: UIButton) {
        performSegue(withIdentifier: "QRScanner", sender: self)
    }
    
    
}
