//
//  BookSellingRecapVC.swift
//  SchoolMarket
//
//  Created by Edward Ducklain on 06/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class BookSellingRecapVC: UIViewController {

    @IBOutlet weak var TitleText: UILabel!
    @IBOutlet weak var ConditionText: UILabel!
    @IBOutlet weak var PriceText: UILabel!
    @IBOutlet weak var FirstImage: UIImageView!
    @IBOutlet weak var SecondImage: UIImageView!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBAction func SendDataButton(_ sender: UIButton) {
        // Here is the code for sending data to the server
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let Model = DataToSend()
        TitleText.text = Model.DataContainer["Titolo"]
        ConditionText.text = Model.DataContainer["Condizione"]
        PriceText.text = Model.DataContainer["Prezzo"]
        DescriptionText.text = Model.DataContainer["Descrizione"]
        FirstImage.image = Model.FirstImage
        SecondImage.image = Model.SecondImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
