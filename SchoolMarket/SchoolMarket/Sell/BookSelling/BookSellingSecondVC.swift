//
//  BookSellingSecondVC.swift
//  SchoolMarket
//
//  Created by Edward Ducklain on 06/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class BookSellingSecondVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    var button = dropDownBtn()
    var Model = DataToSend()
    var checkerForImage = 0
    var checkerForSegue = 0
    let picker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        self.DescriptionText.delegate = self

        //This entire block is for creating the DropDownMenu
        button = dropDownBtn.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        button.setBackgroundImage(#imageLiteral(resourceName: "Drop down menu.png"), for: .normal)
        button.setTitle("Condizioni", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat", size: 20)
        button.setTitleColor(UIColor.black, for: .normal)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        button.contentEdgeInsets = UIEdgeInsets(top: 0,left: 15,bottom: 0,right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 160.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 275).isActive = true
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.setupDropView()
        button.dropView.dropDownOptions = ["Pessime","Buone","Ottime"]
        // Here we have to write which items you can select
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var PriceText: UITextField!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBOutlet weak var FirstImage: UIImageView!
    @IBOutlet weak var SecondImage: UIImageView!
    @IBOutlet weak var FirstLibraryButton: UIButton!
    @IBOutlet weak var SecondLibraryButton: UIButton!
    @IBAction func PhotoFromLibrary1(_ sender: UIButton) {
        checkerForImage = 0
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    @IBAction func PhotoFromLibrary2(_ sender: UIButton) {
        checkerForImage = 1
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    @IBAction func ShootPhoto(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count >= 8 {
            checkerForSegue = 1
        } else {
            checkerForSegue = 0
        }
    }
    
    @IBAction func NextPageButton(_ sender: UIButton) {
        // Data taken from the TextField
        Model.DataContainer["Prezzo"] = PriceText.text
        if checkerForSegue == 1 {
        Model.DataContainer["Descrizione"] = DescriptionText.text
        performSegue(withIdentifier: "BookEnd", sender: self)
        } else {
            let alertVC = UIAlertController(
                title: "Issue",
                message: "Sorry, the description has to have at least 8 characters",
                preferredStyle: .alert)
            let okAction = UIAlertAction(
                title: "OK",
                style:.default,
                handler: nil)
            alertVC.addAction(okAction)
            present(
                alertVC,
                animated: true,
                completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController,
                                     didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        if checkerForImage == 0 {
            FirstImage.contentMode = .scaleAspectFit //3
            FirstImage.image = chosenImage //4
            Model.FirstImage = chosenImage //5
            dismiss(animated:true, completion: nil) //6
            FirstLibraryButton.isHidden = true
        } else if checkerForImage == 1 {
            SecondImage.contentMode = .scaleAspectFit //3
            SecondImage.image = chosenImage //4
            Model.SecondImage = chosenImage //5
            dismiss(animated:true, completion: nil) //6
            SecondLibraryButton.isHidden = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
}


























