//
//  SellLinkVC.swift
//  SchoolMarket
//
//  Created by Marco PC on 09/03/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class SellLinkVC: UIViewController {
    
    private var Model = DataToSend()
    var container : ContainerViewController!
    
    func ColorTheWrite(button: UIButton) {
        button.setTitleColor(UIColor.init(red: 105, green: 151, blue: 176, alpha: 1.0), for: UIControlState.selected)
    }
    
    func performTheSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(name: "Sell", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as UIViewController!
        addChildViewController(vc!)
        view.addSubview((vc?.view)!)
        vc?.didMove(toParentViewController: self)
    }
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "container" {
            self.container = segue.destination as! ContainerViewController
        }
        
    }
    
    @IBAction func BookButton(_ sender: UIButton) {
        Model.WhatIsThePage = 1
        ColorTheWrite(button: sender)
        container.segueIdentifierReceivedFromParent("BookSelling")
    }
    
    @IBAction func NoteButton(_ sender: UIButton) {
        Model.WhatIsThePage = 2
        ColorTheWrite(button: sender)
        container.segueIdentifierReceivedFromParent("NoteSelling")
    }
    
    @IBAction func TutoringButton(_ sender: UIButton) {
        Model.WhatIsThePage = 3
        ColorTheWrite(button: sender)
        container.segueIdentifierReceivedFromParent("TutoringSelling")
    }
    
    @IBAction func ChangePageButton(_ sender: UIButton) {
        if Model.WhatIsThePage == 1 {
            performTheSegue(identifier: "BookSegue")
        } else if Model.WhatIsThePage == 2 {
            performTheSegue(identifier: "NoteSegue")
        } else if Model.WhatIsThePage == 3 {
            performTheSegue(identifier: "TutoringSegue")
        } else {
            self.view.makeToast(message: "Choose one of the three button above please", duration: 2.0, position: HRToastPositionDefault as AnyObject)
        }
    }*/
    
}
