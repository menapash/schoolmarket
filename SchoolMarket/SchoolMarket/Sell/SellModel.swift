//
//  SellModel.swift
//  SchoolMarket
//
//  Created by Edward Ducklain on 06/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit
import Foundation

class DataToSend {
    // Here is the code used for storing the data the user insert and
    // for telling us if he wants to sell notes, books or tutoring
    
    var WhatIsThePage: Int = 0
    var FirstImage: UIImage? = nil
    var SecondImage: UIImage? = nil
    var DataContainer: Dictionary<String,String> = [:]
    
}
