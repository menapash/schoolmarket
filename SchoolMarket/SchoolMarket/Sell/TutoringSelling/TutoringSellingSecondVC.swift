//
//  TutoringSellingSecondVC.swift
//  SchoolMarket
//
//  Created by Edward Ducklain on 06/04/18.
//  Copyright © 2018 Marco PC. All rights reserved.
//

import UIKit

class TutoringSellingSecondVC: UIViewController, UITextViewDelegate {

    var button = dropDownBtn()
    var Model = DataToSend()
    var checkerForSegue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DescriptionText.delegate = self
        
        //This entire block is for creating the DropDownMenu
        button = dropDownBtn.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        button.setBackgroundImage(#imageLiteral(resourceName: "Drop down menu.png"), for: .normal)
        button.setTitle("Condizioni", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat", size: 20)
        button.setTitleColor(UIColor.black, for: .normal)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        button.contentEdgeInsets = UIEdgeInsets(top: 0,left: 15,bottom: 0,right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 160.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 275).isActive = true
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.setupDropView()
        button.dropView.dropDownOptions = ["Pessime","Buone","Ottime"]
        // Here we have to write which items you can select
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count >= 8 {
            checkerForSegue = 1
        } else {
            checkerForSegue = 0
        }
    }
    
    @IBOutlet weak var PriceText: UITextField!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBAction func NextPageButton(_ sender: UIButton) {
        // Data taken from the TextField
        Model.DataContainer["Prezzo"] = PriceText.text
        if checkerForSegue == 1 {
            Model.DataContainer["Descrizione"] = DescriptionText.text
            performSegue(withIdentifier: "TutoringEnd", sender: self)
        } else {
            let alertVC = UIAlertController(
                title: "Issue",
                message: "Sorry, the description has to have at least 8 characters",
                preferredStyle: .alert)
            let okAction = UIAlertAction(
                title: "OK",
                style:.default,
                handler: nil)
            alertVC.addAction(okAction)
            present(
                alertVC,
                animated: true,
                completion: nil)
        }
    }
    
}
